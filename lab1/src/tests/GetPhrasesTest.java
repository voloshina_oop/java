package tests;

import classes.ArgsParser;
import classes.GetPhrases;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.*;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

public class GetPhrasesTest extends Assert {
    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testWrongInputFile() throws IOException {
        thrown.expect(FileNotFoundException.class);
        String[] args = {"example.txt"};
        ArgsParser argsParser = new ArgsParser(args);
        GetPhrases input = new GetPhrases(argsParser);
    }

    @Test
    public void testFile1Input() throws IOException {
        String[] args = {"test1.txt"};
        ArgsParser argsParser = new ArgsParser(args);
        GetPhrases input = new GetPhrases(argsParser);
        List<Entry<String, Integer>> output = new ArrayList<>() {{
            add(new AbstractMap.SimpleEntry<>("yellow yellow ", 2));
            add(new AbstractMap.SimpleEntry<>("yellow cat ", 1));
        }};
        output.sort(Entry.comparingByValue((x, y) -> (-1) * Integer.compare(x, y)));
        assertEquals(input.getPhrasesList(), output);
    }

    @Test
    public void testFile2Input() throws IOException {
        String[] args = {"test2.txt"};
        ArgsParser argsParser = new ArgsParser(args);
        GetPhrases input = new GetPhrases(argsParser);
        List<Entry<String, Integer>> output = new ArrayList<>() {{
            add(new AbstractMap.SimpleEntry<>("the quick ", 4));
            add(new AbstractMap.SimpleEntry<>("quick the ", 3));
            add(new AbstractMap.SimpleEntry<>("fox jumps ", 2));
            add(new AbstractMap.SimpleEntry<>("jumps fox ", 1));
            add(new AbstractMap.SimpleEntry<>("quick fox ", 1));
        }};
        output.sort(Entry.comparingByValue((x, y) -> (-1) * Integer.compare(x, y)));
        assertEquals(input.getPhrasesList(), output);
    }

    @Test
    public void testStandardInput()throws IOException {
        InputStream istream = new FileInputStream("test3.txt");
        System.setIn(istream);
        String[] args = {"-"};
        ArgsParser argsParser = new ArgsParser(args);
        GetPhrases input = new GetPhrases(argsParser);
        List<Entry<String, Integer>> output = new ArrayList<>() {{
            add(new AbstractMap.SimpleEntry<>("One love, ", 2));
            add(new AbstractMap.SimpleEntry<>("one house ", 1));
            add(new AbstractMap.SimpleEntry<>("love, one ", 1));
            add(new AbstractMap.SimpleEntry<>("mouths One ", 1));
            add(new AbstractMap.SimpleEntry<>("love, two ", 1));
            add(new AbstractMap.SimpleEntry<>("two mouths ", 1));
        }};
        output.sort(Entry.comparingByValue((x, y) -> (-1)*Integer.compare(x, y)));
        assertEquals(input.getPhrasesList(), output);
    }
}
