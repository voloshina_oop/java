package tests;

import classes.ArgsParser;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;

public class ArgsParserTest extends Assert {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void testOnlyStandardInput(){
        String[] args = {"-"};
        ArgsParser input = new ArgsParser(args);
        assertEquals(input.getInputFilePresence(), false);
        assertEquals(input.getPhraseLength(), 2);
        assertEquals(input.getRepeatMinCount(), 2);
    }

    @Test
    public void testOnlyPhraseLength(){
        String[] args = {"-n", "3", "-"};
        ArgsParser input = new ArgsParser(args);
        assertEquals(input.getInputFilePresence(), false);
        assertEquals(input.getPhraseLength(), 3);
        assertEquals(input.getRepeatMinCount(), 2);
    }

    @Test
    public void testOnlyRepeatMinCount(){
        String[] args = {"-m", "4", "-"};
        ArgsParser input = new ArgsParser(args);
        assertEquals(input.getInputFilePresence(), false);
        assertEquals(input.getPhraseLength(), 2);
        assertEquals(input.getRepeatMinCount(), 4);
    }

    @Test
    public void testWithoutInputFile(){
        String[] args = {"-m", "4", "-n", "3", "-"};
        ArgsParser input = new ArgsParser(args);
        assertEquals(input.getInputFilePresence(), false);
        assertEquals(input.getPhraseLength(), 3);
        assertEquals(input.getRepeatMinCount(), 4);
    }

    @Test
    public void testOnlyInputFile(){
        String[] args = {"words.txt"};
        ArgsParser input = new ArgsParser(args);
        assertEquals(input.getInputFilePresence(), true);
        assertEquals(input.getSource(), "words.txt");
        assertEquals(input.getPhraseLength(), 2);
        assertEquals(input.getRepeatMinCount(), 2);
    }

    @Test
    public void testAll(){
        String[] args = {"-m", "4", "-n", "3", "words.txt"};
        ArgsParser input = new ArgsParser(args);
        assertEquals(input.getInputFilePresence(), true);
        assertEquals(input.getSource(), "words.txt");
        assertEquals(input.getPhraseLength(), 3);
        assertEquals(input.getRepeatMinCount(), 4);
    }
}
