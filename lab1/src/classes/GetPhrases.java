package classes;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;

public class GetPhrases {
    private HashMap<String, Integer> phrasesMap;
    private List<Entry<String, Integer>> phrasesList;

    private void getMap(ArgsParser parser)throws FileNotFoundException {
        phrasesMap = new HashMap<>();
        Scanner in;
        if(parser.getInputFilePresence()){
                in = new Scanner(new File(parser.getSource()));
        }
        else {
            in = new Scanner(System.in);
        }
        Vector<String> curPhrase = new Vector<>();
        int length = parser.getPhraseLength();
        int curPhraseLength = 0;
        while(in.hasNext()){
            String curWord = "";
            curWord += in.next() + ' ';                                      //TODO: exception should be checked?
            curPhrase.add(curWord);
            curPhraseLength++;
            if(curPhraseLength == length){
                StringBuilder phrase = new StringBuilder("");
                for (String aCurPhrase : curPhrase) phrase.append(aCurPhrase);
                if(phrasesMap.containsKey(phrase.toString()))
                    phrasesMap.replace(phrase.toString(), (phrasesMap.get(phrase.toString())+1));
                else phrasesMap.put(phrase.toString(), 1);
                curPhraseLength--;
                curPhrase.remove(0);
            }
        }
        in.close();
    }

    public GetPhrases(ArgsParser parser) throws FileNotFoundException {
        this.getMap(parser);
        Set<Entry<String, Integer>> phrasesSet  = phrasesMap.entrySet();
        phrasesList = new ArrayList<>(phrasesSet);
        phrasesList.sort(Entry.comparingByValue((x, y) -> (-1)*Integer.compare(x, y)));
    }

    public List<Entry<String, Integer>> getPhrasesList(){
        return phrasesList;
    }
}
