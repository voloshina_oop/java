package classes;

import java.util.List;
import java.util.Map;

public class IO {
    IO(){

    }

    public void printPhrases(List<Map.Entry<String, Integer>> entry, Integer repeats){
        for(Map.Entry<String, Integer> elem: entry){
            if(elem.getValue() >= repeats)
                System.out.println(elem.getKey() + "(" + elem.getValue() + ")");
        }
    }
}
