package classes;

public class ArgsParser{
    private int phraseLength;
    private int repeatMinCount;
    private boolean inputFilePresence;
    private String source;

    public ArgsParser(String[] args){
        int length = args.length;
        phraseLength = 2;
        repeatMinCount = 2;
        if(args.length == 0) inputFilePresence = false;
        for(int i = 0; i < length; i++){
            try{
                if(args[i].equals("-n")){
                    if((i+1) <= length)
                        phraseLength = Integer.parseInt(args[i+1]);
                    else throw new NumberFormatException();
                }
            } catch (NumberFormatException e){
                System.err.println("Format arguments error. Length of phrase will be set by default");
            }
            try{
                if(args[i].equals("-m")){
                    if((i+1) <= length)
                        repeatMinCount = Integer.parseInt(args[i+1]);
                    else throw new NumberFormatException();
                }
            } catch (NumberFormatException e){
                System.err.println("Format arguments error. Minimum number of repetitions will be set by default");
            }
        }
        if((args.length != 0)&&(!args[length - 1].equals("-"))){
            inputFilePresence = true;
            source = args[length - 1];
        }
        else inputFilePresence = false;
    }

     public int getPhraseLength(){
        return phraseLength;
    }

    public int getRepeatMinCount(){
        return repeatMinCount;
    }

    public boolean getInputFilePresence(){
        return inputFilePresence;
    }

    public String getSource(){
        return source;
    }
}
