package classes;

import java.io.FileNotFoundException;
import java.io.IOException;

public class PhrasesCounter{
    public static void main(String[] args) {
        ArgsParser argsParser;
        argsParser = new ArgsParser(args);
        try {
            GetPhrases GP = new GetPhrases(argsParser);
            IO output = new IO();
            output.printPhrases(GP.getPhrasesList(), argsParser.getRepeatMinCount());
        } catch (FileNotFoundException e) {
            System.err.println("File" + " " + argsParser.getSource() + " " + "doesn't exist. The program will be terminated.");
        }
    }
}
