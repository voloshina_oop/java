package ru.nsu.ccfit.voloshina.task3.Model;

import ru.nsu.ccfit.voloshina.task3.Exceptions.HighScoreFileAccessingException;
import ru.nsu.ccfit.voloshina.task3.Listener;
import ru.nsu.ccfit.voloshina.task3.Pair;
import ru.nsu.ccfit.voloshina.task3.Model.Shapes.*;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class Model {
    private ArrayList<ArrayList<CellStates>> field;
    private Shape curShape;
    private Shape nextShape;
    private int widthSize = 10;
    private int heightSize = 20;
    private long curScore = 0L;
    private long highScore;
    private int level = 1;

    private final HashMap<Integer, Shape> shapes;
    private static Pair<Integer, Integer> curInitialCell = new Pair<>(0, 0);

    private ArrayList<Listener> listeners;

    public Model() throws HighScoreFileAccessingException {
        field = new ArrayList<>();
        for(int i = 0; i < heightSize; i++){
            ArrayList<CellStates> inner = new ArrayList<>();
            for (int j = 0; j < widthSize; j++)
                inner.add(CellStates.Empty);
            field.add(inner);
        }

        shapes = new HashMap<Integer, Shape>() {{
            put(0, new JShape());
            put(1, new LShape());
            put(2, new OShape());
            put(3, new SShape());
            put(4, new TShape());
            put(5, new ZShape());
            put(6, new IShape());
        }};
        curShape = new Shape();
        nextShape = new Shape();
        curShape = generateShape(curShape);
        nextShape = generateShape(curShape);

        setCurInitialCell(-1, (widthSize /2 - curShape.getRectSize()/2));

        listeners = new ArrayList<>();

        try(Scanner highScoreReader = new Scanner(new FileReader("HighScore.txt"))){
            highScore = highScoreReader.nextInt();
        }
        catch (FileNotFoundException ex){
            throw new HighScoreFileAccessingException("High score can't be red.\n");
        }
    }

    private Shape generateShape(Shape prevShape){
        final Random random = new Random();
        Shape shape;
        do {
            shape = shapes.get(random.nextInt(7));
        } while(shape.getClass().getName().equals(prevShape.getClass().getName()));
        return shape;
    }

    public ArrayList<ArrayList<CellStates>> getField() throws Exception{
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream ous = new ObjectOutputStream(baos);
        ous.writeObject(field);
        ous.close();
        ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
        ObjectInputStream ois = new ObjectInputStream(bais);
        return (ArrayList<ArrayList<CellStates>>)ois.readObject();
    }

    public Pair<Integer, Integer> getCurInitialCell(){
        return curInitialCell;
    }

    public void setCurInitialCell(Integer x ,Integer y){
        curInitialCell.setKey(x);
        curInitialCell.setValue(y);
    }

    public Integer getWidthSize() {
        return widthSize;
    }

    public Integer getHeightSize() {
        return heightSize;
    }

    public Shape getCurShape() {
        return curShape;
    }

    public ArrayList<ArrayList<CellStates>> getNextShape() {
        return nextShape.getStates().get(0);
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getCurScore() {
        return curScore;
    }

    public long getHighScore() {
        return highScore;
    }

    public void setHighScore() throws HighScoreFileAccessingException {
        if(curScore > highScore){
            try(FileWriter highScoreFW = new FileWriter("HighScore.txt")){
                highScoreFW.write(((Long)curScore).toString());
            }
            catch (IOException ex){
                throw new HighScoreFileAccessingException("New high score can't be wrote.\n");
            }
        }
    }

    public void getNewShapes(){
        curShape = nextShape;
        nextShape = generateShape(curShape);
        curShape.setCurState(0);
    }

    public void putShape(){
        drawShape(field);
    }

    public boolean checkLine(){
        int x = curInitialCell.getKey();
        int rectSize = curShape.getRectSize();
        boolean success = false;

        for (int i = 0; i<rectSize; i++){
            if((i+x < 0)||(i+x >= heightSize)) continue;
            int notEmptyCounts = 0;
            for (int j = 0; j < widthSize; j++){
                if(j >= widthSize) continue;
                if(field.get(i+x).get(j) != CellStates.Empty){
                    notEmptyCounts++;
                }
            }
            if (notEmptyCounts == widthSize){
                field.remove(i+x);
                ArrayList<CellStates> inner = new ArrayList<>();
                for (int k = 0; k < widthSize; k++)
                    inner.add(CellStates.Empty);
                field.add(0, inner);
                curScore += 100;
                success = true;
            }
        }
        return success;
    }

    public boolean checkUpdates(){
        int x = curInitialCell.getKey();
        int y = curInitialCell.getValue();
        setCurInitialCell(x, y);

        int size = curShape.getRectSize();
        ArrayList<ArrayList<CellStates>> shape = curShape.getStates().get(curShape.getCurState());

        for(int i = 0; i < size; i++){
            if(i+x < 0) continue;
            for (int j = 0; j < size; j++){
                if(shape.get(i).get(j) != CellStates.Empty){
                    if(!((j+y >= 0)&&(i+x < heightSize)&&(j+y < widthSize)&&(field.get(i+x).get(j+y) == CellStates.Empty))) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private void drawShape(ArrayList<ArrayList<CellStates>> field){
        int x = curInitialCell.getKey();
        int y = curInitialCell.getValue();

        int rectSize = curShape.getRectSize();
        int curState = curShape.getCurState();

        ArrayList<ArrayList<ArrayList<CellStates>>> states = curShape.getStates();

        for(int i = 0; i < rectSize; i++){
            if((i+x < 0)||(i+x >= heightSize)) continue;
                ArrayList<CellStates> tmp = field.get(i+x);
            for (int j = 0; j < rectSize; j++){
                if((j+y < 0)||(j+y >= widthSize)) continue;
                if(states.get(curState).get(i).get(j) != CellStates.Empty)
                    tmp.set(j+y, states.get(curState).get(i).get(j));
            }
            field.set(i+x, tmp);
        }
    }

    public ArrayList<ArrayList<CellStates>> drawField() throws Exception{
        ArrayList<ArrayList<CellStates>> tmpField = getField();
        drawShape(tmpField);
        return tmpField;
    }

    public void cleanField(){
        for(int i = 0; i < heightSize; i++){
            for (int j = 0; j < widthSize; j++) {
                field.get(i).remove(j);
                field.get(i).add(j, CellStates.Empty);
            }
        }
    }

    public void subscribe(Listener listener){
        if(!listeners.contains(listener)){
            listeners.add(listener);
        }
    }

    public void unsubscribe(Listener listener){
        if(!listeners.contains(listener)){
            listeners.remove(listener);
        }
    }

    public void allNotify(){
        for(Listener listener: listeners){
            listener.update();
        }
    }
}
