package ru.nsu.ccfit.voloshina.task3.Model;

import java.awt.*;

public enum CellStates {
    Empty(0), Busy(1), Red(2), Magenta(3), Yellow(4), Cyan(5), Blue(6), Orange(7), Green(8);

    private final Integer value;

    CellStates(Integer value){
        this.value = value;
    }

    public Integer value(){
        return this.value;
    }

    public boolean compare(CellStates another){
        return (this == CellStates.Empty) && (another == CellStates.Empty);
    }

    public Color color(){
        switch (this.value) {
            case 0: return Color.lightGray;
            case 1: return Color.darkGray;
            case 2: return Color.red;
            case 3: return Color.magenta;
            case 4: return Color.yellow;
            case 5: return Color.cyan;
            case 6: return Color.blue;
            case 7: return Color.orange;
            case 8: return Color.green;

            default: return Color.gray;
        }
    }
}

