package ru.nsu.ccfit.voloshina.task3.Model.Shapes;

import ru.nsu.ccfit.voloshina.task3.Model.CellStates;

import java.util.ArrayList;

public class OShape extends Shape {

    public OShape(){
        ArrayList<ArrayList<CellStates>> state= new ArrayList<ArrayList<CellStates>>(){{
            add(0, new ArrayList<CellStates>(){{
                add(0, CellStates.Blue);
                add(1, CellStates.Blue);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
            add(1, new ArrayList<CellStates>(){{
                add(0, CellStates.Blue);
                add(1, CellStates.Blue);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
            add(2, new ArrayList<CellStates>(){{
                add(0, CellStates.Empty);
                add(1, CellStates.Empty);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
            add(3, new ArrayList<CellStates>(){{
                add(0, CellStates.Empty);
                add(1, CellStates.Empty);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
        }};

        for(int i = 0; i < statesCounts; i++){
            states.add(i, state);
        }
    }

}
