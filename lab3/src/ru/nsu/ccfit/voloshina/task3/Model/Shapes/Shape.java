package ru.nsu.ccfit.voloshina.task3.Model.Shapes;

import ru.nsu.ccfit.voloshina.task3.Model.CellStates;

import java.util.ArrayList;

public class Shape {
    final int statesCounts = 4;
    private final int rectSize = 4;

    ArrayList<ArrayList<ArrayList<CellStates>>> states = new ArrayList<>();
    private int curState = 0;

    public int getCurState(){
        return curState;
    }

    public void setCurState(int curState) {
        this.curState = curState;
    }

    public int getRectSize(){
        return rectSize;
    }

    public ArrayList<ArrayList<ArrayList<CellStates>>> getStates() {
        return states;
    }

    public void printShape(){
        for(int i = 0; i < rectSize; i++){
            for(int k = 0; k < statesCounts; k++){
                for (int j = 0; j < rectSize; j++){
                    System.out.print((states.get(k).get(i).get(j).value()).toString()+' ');
                }
                System.out.print("\t");
            }
            System.out.println();
        }
        System.out.println();
    }

    public void turnRight(){
        if(curState+1 < statesCounts)
            curState++;
        else curState = 0;
    }

    public void turnLeft(){
        if(curState-1 > 0)
            curState--;
        else curState = 3;
    }
}
