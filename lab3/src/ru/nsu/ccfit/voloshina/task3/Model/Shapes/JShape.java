package ru.nsu.ccfit.voloshina.task3.Model.Shapes;

import ru.nsu.ccfit.voloshina.task3.Model.CellStates;

import java.util.ArrayList;

public class JShape extends Shape {

    public JShape(){
        ArrayList<ArrayList<CellStates>> zeroState = new ArrayList<ArrayList<CellStates>>(){{
            add(0, new ArrayList<CellStates>(){{
                add(0, CellStates.Yellow);
                add(1, CellStates.Empty);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
            add(1, new ArrayList<CellStates>(){{
                add(0, CellStates.Yellow);
                add(1, CellStates.Yellow);
                add(2, CellStates.Yellow);
                add(3, CellStates.Empty);
            }});
            add(2, new ArrayList<CellStates>(){{
                add(0, CellStates.Empty);
                add(1, CellStates.Empty);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
            add(3, new ArrayList<CellStates>(){{
                add(0, CellStates.Empty);
                add(1, CellStates.Empty);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
        }};

        ArrayList<ArrayList<CellStates>> firstState = new ArrayList<ArrayList<CellStates>>(){{
            add(0, new ArrayList<CellStates>(){{
                add(0, CellStates.Empty);
                add(1, CellStates.Yellow);
                add(2, CellStates.Yellow);
                add(3, CellStates.Empty);
            }});
            add(1, new ArrayList<CellStates>(){{
                add(0, CellStates.Empty);
                add(1, CellStates.Yellow);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
            add(2, new ArrayList<CellStates>(){{
                add(0, CellStates.Empty);
                add(1, CellStates.Yellow);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
            add(3, new ArrayList<CellStates>(){{
                add(0, CellStates.Empty);
                add(1, CellStates.Empty);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
        }};

        ArrayList<ArrayList<CellStates>> secondState = new ArrayList<ArrayList<CellStates>>(){{
            add(0, new ArrayList<CellStates>(){{
                add(0, CellStates.Empty);
                add(1, CellStates.Empty);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
            add(1, new ArrayList<CellStates>(){{
                add(0, CellStates.Yellow);
                add(1, CellStates.Yellow);
                add(2, CellStates.Yellow);
                add(3, CellStates.Empty);
            }});
            add(2, new ArrayList<CellStates>(){{
                add(0, CellStates.Empty);
                add(1, CellStates.Empty);
                add(2, CellStates.Yellow);
                add(3, CellStates.Empty);
            }});
            add(3, new ArrayList<CellStates>(){{
                add(0, CellStates.Empty);
                add(1, CellStates.Empty);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
        }};

        ArrayList<ArrayList<CellStates>> thirdState = new ArrayList<ArrayList<CellStates>>(){{
            add(0, new ArrayList<CellStates>(){{
                add(0, CellStates.Empty);
                add(1, CellStates.Yellow);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
            add(1, new ArrayList<CellStates>(){{
                add(0, CellStates.Empty);
                add(1, CellStates.Yellow);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
            add(2, new ArrayList<CellStates>(){{
                add(0, CellStates.Yellow);
                add(1, CellStates.Yellow);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
            add(3, new ArrayList<CellStates>(){{
                add(0, CellStates.Empty);
                add(1, CellStates.Empty);
                add(2, CellStates.Empty);
                add(3, CellStates.Empty);
            }});
        }};

        states.add(zeroState);
        states.add(firstState);
        states.add(secondState);
        states.add(thirdState);
    }
}
