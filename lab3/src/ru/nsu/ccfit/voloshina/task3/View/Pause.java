package ru.nsu.ccfit.voloshina.task3.View;

import ru.nsu.ccfit.voloshina.task3.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Pause extends JDialog {
    private JLabel pauseMsg = new JLabel("PAUSE");
    private JLabel aboutKeyMsg = new JLabel("Press \"Enter\" to continue");
    private JButton ok = new JButton("OK");
    private Controller controller;

    Pause(JFrame owner, Controller controller){
        super(owner, "Pause", true);
        this.setBounds(500,250,300,250);

        this.controller = controller;
        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints constraints =  new GridBagConstraints();
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.fill   = GridBagConstraints.NONE;
        constraints.gridheight = 1;
        constraints.gridwidth  = GridBagConstraints.REMAINDER;
        constraints.gridx = GridBagConstraints.RELATIVE;
        constraints.gridy = GridBagConstraints.RELATIVE;
        constraints.insets = new Insets(20, 20, 20, 20);
        constraints.ipadx = 0;
        constraints.ipady = 0;
        constraints.weightx = 0.0;
        constraints.weighty = 0.0;
        getContentPane().setLayout(gbl);

        gbl.setConstraints(pauseMsg, constraints);
        constraints.insets.top = 10;
        gbl.setConstraints(aboutKeyMsg, constraints);
        constraints.insets.top = 30;
        gbl.setConstraints(ok, constraints);
        ok.addKeyListener(new OkButtonKeyListener());

        pauseMsg.setFont(new Font("Serif", Font.BOLD, 20));

        getContentPane().add(pauseMsg);
        getContentPane().add(aboutKeyMsg);
        getContentPane().add(ok);

        getContentPane().addKeyListener(new OkButtonKeyListener());

        setVisible(true);
    }

    class OkButtonKeyListener extends KeyAdapter {
        public void keyTyped(KeyEvent event) {
            if (event.getKeyChar() == KeyEvent.VK_ENTER) {
                controller.startTimer();
                dispose();
            }
        }
    }
}
