package ru.nsu.ccfit.voloshina.task3.View;

import ru.nsu.ccfit.voloshina.task3.Controller;
import ru.nsu.ccfit.voloshina.task3.Exceptions.HighScoreFileAccessingException;
import ru.nsu.ccfit.voloshina.task3.Listener;
import ru.nsu.ccfit.voloshina.task3.Model.CellStates;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class GUI extends JFrame implements Listener{

    private Controller controller = new Controller();
    private Level level = new Level();
    private Score score = new Score();
    private HighScore highScore = new HighScore();
    private NextShape nextShape = new NextShape();
    private MainField field = new MainField();
    private JButton back = new JButton("Back");

    private long curScore;
    private Long curHighScore;


    GUI() throws HighScoreFileAccessingException {
        super("Tetris");
        this.setBounds(500,200,400,350);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        controller.subscribeToModel(this);

        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints constraints =  new GridBagConstraints();
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.fill   = GridBagConstraints.NONE;
        constraints.gridheight = 1;
        constraints.gridwidth  = GridBagConstraints.REMAINDER;
        constraints.gridx = GridBagConstraints.RELATIVE;
        constraints.gridy = GridBagConstraints.RELATIVE;
        constraints.insets = new Insets(5, 0, 5, 0);
        constraints.ipadx = 0;
        constraints.ipady = 0;
        constraints.weightx = 0.0;
        constraints.weighty = 0.0;

        gbl.setConstraints(level, constraints);
        gbl.setConstraints(score, constraints);
        constraints.ipadx = 70;
        gbl.setConstraints(highScore, constraints);
        constraints.ipadx = 50;
        constraints.ipady = 50;
        gbl.setConstraints(nextShape, constraints);

        back.addKeyListener(new BackButtonKeyListener());
        back.addMouseListener(new BackButtonMouseListener());
        constraints.ipadx = 0;
        constraints.ipady = 0;
        constraints.insets.top = 40;
        gbl.setConstraints(back, constraints);

        JPanel information = new JPanel(gbl);
        information.add(level);
        information.add(score);
        information.add(highScore);
        information.add(nextShape);
        information.add(back);

        setLayout(new GridLayout(0,2,2,2));
        add(field);
        add(information);

        pack();
        setResizable(false);
        setVisible(true);
    }

    @Override
    public void update() {
        if(controller.isGameOver()){
            boolean showScore = false;
            if(curScore >= curHighScore){
                showScore = true;
            }
            new GameOver(curScore, showScore, this);
            dispose();
        }
    }

    class MainField extends JPanel implements Listener {
        private int squareW = 15;
        private int squareH = 15;

        MainField(){
            setBorder(BorderFactory.createLineBorder(Color.black));
            setFocusable(true);

            controller.subscribeToModel(this);

            addKeyListener(new KeyAdapter() {
                public void keyTyped(KeyEvent event) {
                    if(event.getKeyChar() == 'a'){
                        controller.leftShift();
                    }
                    if(event.getKeyChar() == 'd'){
                        controller.rightShift();
                    }
                    if(event.getKeyChar() == 's'){
                        try{
                            controller.downShift();
                        }
                        catch (HighScoreFileAccessingException ex){
                            ex.printStackTrace();                                                //TODO: Error Window!!
                        }
                    }
                    if(event.getKeyChar() == 'w'){
                        try{
                            controller.rotateRight();
                        }
                        catch (Exception ex){
                            ex.printStackTrace();                                                //TODO: Error Window!!
                        }
                    }
                }
            });
            addKeyListener(new KeyAdapter() {
                public void keyTyped(KeyEvent event) {
                    if (event.getKeyChar() == KeyEvent.VK_ESCAPE) {
                        controller.stopTimer();
                        new Pause(GUI.this, controller);
                    }
                }
            });
        }

        protected void paintComponent(Graphics g) {
            super.paintComponent(g);

            try{
                ArrayList<ArrayList<CellStates>> field = controller.getField();
                for(int i = 0; i < controller.getFieldHeight(); i++){
                    for (int j = 0; j < controller.getFieldWidth(); j++){
                        g.setColor(field.get(i).get(j).color());
                        g.fillRect(j*squareW, i*squareH, squareW, squareH);
                        g.setColor(Color.gray);
                        g.drawRect(j*squareW, i*squareH, squareW, squareH);
                    }
                }
            }
            catch (Exception ex){
                ex.printStackTrace();                                                            //TODO: Error Window!!
            }
        }

        public Dimension getPreferredSize() {
            return new Dimension(150,300);
        }


        @Override
        public void update() {
            repaint();
        }
    }

    class Level extends JPanel implements Listener {
        JLabel text = new JLabel("level:");
        JLabel variable = new JLabel(controller.getLevel().toString());
        int level;

        Level(){
            add(text);
            add(variable);
            controller.subscribeToModel(this);
        }

        @Override
        public void update() {
            Integer newLevel = controller.getLevel();
            if(newLevel > level){
                level = newLevel;
                variable.setText(newLevel.toString());
                repaint();
            }
        }
    }

    class Score extends JPanel implements Listener {
        JLabel text = new JLabel("score:");
        JLabel variable = new JLabel(controller.getCurScore().toString());

        Score(){
            add(text);
            add(variable);
            controller.subscribeToModel(this);
        }

        @Override
        public void update() {
            Long newScore = controller.getCurScore();
            if(newScore > curScore){
                curScore = newScore;
                variable.setText(newScore.toString());
                repaint();
            }
        }
    }

    class HighScore extends JPanel implements Listener {
        JLabel text = new JLabel("high score:");
        JLabel variable = new JLabel();

        HighScore(){
            curHighScore = controller.getHighScore();
            variable.setText(curHighScore.toString());
            add(text);
            add(variable);
            controller.subscribeToModel(this);
        }

        public Dimension getPreferredSize() {
            return new Dimension(60,60);
        }

        @Override
        public void update() {
            Long newScore = controller.getCurScore();
            if(newScore > curHighScore){
                curHighScore = newScore;
                variable.setText(newScore.toString());
                repaint();
            }
        }
    }

    class NextShape extends JPanel implements Listener{
        int shapeSize = 4;

        private int squareW = 15;
        private int squareH = 15;

        NextShape(){
            setBorder(BorderFactory.createLineBorder(Color.black));
            controller.subscribeToModel(this);
        }

        protected void paintComponent(Graphics g){
            super.paintComponent(g);

            ArrayList<ArrayList<CellStates>> nextShape = controller.getNextShape();
            for(int i = 0; i < shapeSize; i++) {
                for (int j = 0; j < shapeSize; j++) {
                    g.setColor(nextShape.get(i).get(j).color());
                    g.fillRect(j*squareW, i*squareH, squareW, squareH);
                    g.setColor(Color.gray);
                    g.drawRect(j*squareW, i*squareH, squareW, squareH);
                }
            }
        }

        @Override
        public void update() {
            repaint();
        }
    }

    class BackButtonMouseListener extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent event) {
            controller.stopTimer();
            Welcome app = new Welcome();
            app.setVisible(true);
            dispose();
        }
    }

    class BackButtonKeyListener extends KeyAdapter {
        public void keyTyped(KeyEvent event) {
            if (event.getKeyChar() == KeyEvent.VK_ENTER) {
                controller.stopTimer();
                Welcome app = new Welcome();
                app.setVisible(true);
                dispose();
            }
        }
    }
}
