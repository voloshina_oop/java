package ru.nsu.ccfit.voloshina.task3.View;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

class Records extends JFrame {
    Records(){
        super("Records");
        this.setBounds(500,250,300,250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints constraints =  new GridBagConstraints();
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.fill   = GridBagConstraints.NONE;
        constraints.gridheight = 1;
        constraints.gridwidth  = GridBagConstraints.REMAINDER;
        constraints.gridx = GridBagConstraints.RELATIVE;
        constraints.gridy = GridBagConstraints.RELATIVE;
        constraints.insets = new Insets(20, 20, 20, 20);
        constraints.ipadx = 0;
        constraints.ipady = 0;
        constraints.weightx = 0.0;
        constraints.weighty = 0.0;

        getContentPane().setLayout(gbl);

        ArrayList<String> records = new ArrayList<>();

        try(Scanner highScoresTableReader = new Scanner(new FileReader("HighScoresTable.txt")).useDelimiter("\n")){
            while(highScoresTableReader.hasNext()){
                records.add(highScoresTableReader.next());
            }
        }
        catch (FileNotFoundException ex){
            new ErrorWindow("Can't read \"HighScoresTable.txt\" file.\n");
            dispose();
        }

        JTable tbl = new JTable(new RecordsTableModel(records));
        JScrollPane jscrlp = new JScrollPane(tbl);
        tbl.setPreferredScrollableViewportSize(new Dimension(250, 100));
        gbl.setConstraints(jscrlp, constraints);

        JButton mainMenu = new JButton("Back to Main Menu");
        gbl.setConstraints(mainMenu, constraints);
        mainMenu.addKeyListener(new BackButtonKeyListener());
        mainMenu.addMouseListener(new BackButtonMouseListener());

        getContentPane().add(jscrlp);
        getContentPane().add(mainMenu);

        pack();
        setResizable(false);
        setVisible(true);
    }

    class RecordsTableModel extends AbstractTableModel {
        ArrayList<String> records;

        RecordsTableModel(ArrayList<String> records){
            this.records = records;
        }

        @Override
        public int getRowCount() {
            return records.size();
        }
        @Override
        public int getColumnCount() {
            return 2;
        }

        @Override
        public String getColumnName(int c) {
            String result = "";
            switch (c) {
                case 0:
                    result = "Score";
                    break;
                case 1:
                    result = "Player name";
                    break;
            }
            return result;
        }

        @Override
        public Object getValueAt(int row, int column) {
            String[] arr = records.get(row).split(" ");
            return arr[column];
        }
    }

    class BackButtonMouseListener extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent event) {
            Welcome app = new Welcome();
            app.setVisible(true);
            dispose();
        }
    }

    class BackButtonKeyListener extends KeyAdapter {
        public void keyTyped(KeyEvent event) {
            if (event.getKeyChar() == KeyEvent.VK_ENTER) {
                Welcome app = new Welcome();
                app.setVisible(true);
                dispose();
            }
        }
    }
}
