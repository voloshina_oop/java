package ru.nsu.ccfit.voloshina.task3.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

class Settings extends JFrame {
    private int easyLevel = 500;
    private int mediumLevel = 300;
    private int hardLevel = 100;
    private String[] arr;

    JRadioButton easy = new JRadioButton("Easy");
    JRadioButton medium = new JRadioButton("Medium");
    JRadioButton hard = new JRadioButton("Hard");

    Settings(){
        super("Settings");
        this.setBounds(500,250,300,250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints constraints =  new GridBagConstraints();
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.fill   = GridBagConstraints.NONE;
        constraints.gridheight = 1;
        constraints.gridwidth  = GridBagConstraints.REMAINDER;
        constraints.gridx = GridBagConstraints.RELATIVE;
        constraints.gridy = GridBagConstraints.RELATIVE;
        constraints.insets = new Insets(20, 20, 10, 20);
        constraints.ipadx = 0;
        constraints.ipady = 0;
        constraints.weightx = 0.0;
        constraints.weighty = 0.0;
        getContentPane().setLayout(gbl);

        JLabel difficulty = new JLabel("Level of difficulty: ");
        gbl.setConstraints(difficulty, constraints);

        easy.setFont(new Font("Arial", Font.PLAIN, 12));
        medium.setFont(new Font("Arial", Font.PLAIN, 12));
        hard.setFont(new Font("Arial", Font.PLAIN, 12));

        try(Scanner settingsReader = new Scanner(new FileReader("Settings.txt"))){
            while(settingsReader.hasNextLine()){
                arr = settingsReader.nextLine().split(" ");
                if(arr[0].equals("scoreForSpeedUp")){
                    int curScoreForSpeedUp = Integer.parseInt(arr[2]);
                    if(curScoreForSpeedUp == easyLevel) {
                        easy.setSelected(true);
                        easy.setFont(new Font("Serif", Font.ITALIC, 12));
                    }
                    if(curScoreForSpeedUp == mediumLevel) {
                        medium.setFont(new Font("Serif", Font.ITALIC, 12));
                        medium.setSelected(true);
                    }
                    if(curScoreForSpeedUp == hardLevel) {
                        hard.setFont(new Font("Serif", Font.ITALIC, 12));
                        hard.setSelected(true);
                    }
                    break;
                }
            }
        }
        catch (FileNotFoundException ex){
            new ErrorWindow("Can't read \"Settings\" file.\n");
            dispose();
        }

        easy.addMouseListener(new EasyLevelButtonMouseListener());
        easy.addKeyListener(new EasyLevelButtonKeyListener());
        medium.addMouseListener(new MediumLevelButtonMouseListener());
        medium.addKeyListener(new MediumLevelButtonKeyListener());
        hard.addMouseListener(new HardLevelButtonMouseListener());
        hard.addKeyListener(new HardLevelButtonKeyListener());

        JPanel buttonPanel = new JPanel();
        ButtonGroup group = new ButtonGroup();
        group.add(easy);
        group.add(medium);
        group.add(hard);
        buttonPanel.add(easy);
        buttonPanel.add(medium);
        buttonPanel.add(hard);
        gbl.setConstraints(buttonPanel, constraints);

        JButton mainMenu = new JButton("Back to Main Menu");
        gbl.setConstraints(mainMenu, constraints);
        mainMenu.addKeyListener(new BackButtonKeyListener());
        mainMenu.addMouseListener(new BackButtonMouseListener());

        getContentPane().add(difficulty);
        getContentPane().add(buttonPanel);
        getContentPane().add(mainMenu);

        setVisible(true);
    }

    private void writeSetting(){
        try (FileWriter highScoresTableFW = new FileWriter("Settings.txt")){
            highScoresTableFW.write(arr[0]+ " " + arr[1] + " " + arr[2]);
        }
        catch (IOException ex){
            new ErrorWindow("New difficulty level can't be wrote to \"Settings\" file.\n");
            dispose();
        }
    }

    private void easyClickedReaction(){
        easy.setFont(new Font("Serif", Font.ITALIC, 12));
        medium.setFont(new Font("Arial", Font.PLAIN, 12));
        hard.setFont(new Font("Arial", Font.PLAIN, 12));
        arr[2] = String.valueOf(easyLevel);
        writeSetting();
    }

    private void mediumClickedReaction(){
        easy.setFont(new Font("Arial", Font.PLAIN, 12));
        medium.setFont(new Font("Serif", Font.ITALIC, 12));
        hard.setFont(new Font("Arial", Font.PLAIN, 12));
        arr[2] = String.valueOf(mediumLevel);
        writeSetting();
    }

    private void hardClickedReaction(){
        easy.setFont(new Font("Arial", Font.PLAIN, 12));
        medium.setFont(new Font("Arial", Font.PLAIN, 12));
        hard.setFont(new Font("Serif", Font.ITALIC, 12));
        arr[2] = String.valueOf(hardLevel);
        writeSetting();
    }

    class EasyLevelButtonMouseListener extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent event) {
            easyClickedReaction();
        }
    }

    class MediumLevelButtonMouseListener extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent event) {
            mediumClickedReaction();
        }
    }

    class HardLevelButtonMouseListener extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent event) {
            hardClickedReaction();
        }
    }

    class EasyLevelButtonKeyListener extends KeyAdapter {
        public void keyTyped(KeyEvent event) {
            if (event.getKeyChar() == KeyEvent.VK_ENTER) {
                easyClickedReaction();
            }
        }
    }

    class MediumLevelButtonKeyListener extends KeyAdapter {
        public void keyTyped(KeyEvent event) {
            if (event.getKeyChar() == KeyEvent.VK_ENTER) {
                mediumClickedReaction();
            }
        }
    }

    class HardLevelButtonKeyListener extends KeyAdapter {
        public void keyTyped(KeyEvent event) {
            if (event.getKeyChar() == KeyEvent.VK_ENTER) {
                hardClickedReaction();
            }
        }
    }

    class BackButtonMouseListener extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent event) {
            Welcome app = new Welcome();
            app.setVisible(true);
            dispose();
        }
    }

    class BackButtonKeyListener extends KeyAdapter {
        public void keyTyped(KeyEvent event) {
            if (event.getKeyChar() == KeyEvent.VK_ENTER) {
                Welcome app = new Welcome();
                app.setVisible(true);
                dispose();
            }
        }
    }
}
