package ru.nsu.ccfit.voloshina.task3.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class GameOver extends JFrame {
    private JLabel looseMsg = new JLabel("GAME OVER");
    private JLabel enterNameAsk = new JLabel("Please, enter you're name: ");
    private JTextField name = new JTextField("", 15);
    private JLabel score = new JLabel();
    private JButton ok = new JButton("OK");

    private long playerScore;
    private boolean playerWin;

    GameOver(Long plrScore, boolean win, JFrame main){
        super("Game Over");

        playerScore = plrScore;
        playerWin = win;
        looseMsg.setFont(new Font("Serif", Font.BOLD, 20));

        this.setBounds(500,250,300,250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints c =  new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill   = GridBagConstraints.NONE;
        c.gridheight = 1;
        c.gridwidth  = GridBagConstraints.REMAINDER;
        c.gridx = GridBagConstraints.RELATIVE;
        c.gridy = GridBagConstraints.RELATIVE;
        c.insets = new Insets(3, 20, 10, 20);
        c.ipadx = 0;
        c.ipady = 0;
        c.weightx = 0.0;
        c.weighty = 0.0;

        gbl.setConstraints(looseMsg, c);
        c.insets.bottom = 3;
        gbl.setConstraints(enterNameAsk, c);
        gbl.setConstraints(name, c);
        c.insets.top = 8;
        gbl.setConstraints(score, c);
        gbl.setConstraints(ok, c);

        Container container = this.getContentPane();
        container.setLayout(gbl);
        container.add(looseMsg);
        if(win){
            container.add(enterNameAsk);
            name.addKeyListener(new OKButtonKeyListener());
            container.add(name);
        }

        score.setText("Your score: " + plrScore.toString());
        container.add(score);

        ok.addMouseListener(new OKButtonMouseListener());
        ok.addKeyListener(new OKButtonKeyListener());
        container.add(ok);

        setVisible(true);
    }

    private void goToMainMenu(){
        Welcome app = new Welcome();
        app.setVisible(true);
        this.dispose();
    }

    private void addRecord(Long playerScore, String playerName) {
        ArrayList<String> top = new ArrayList<>(11);
        try (Scanner highScoreScan = new Scanner(new File("HighScoresTable.txt")).useDelimiter("\n")){
            while(highScoreScan.hasNext())
                top.add(highScoreScan.next());

            String newScore = "";
            newScore = newScore.concat(playerScore.toString()).concat(" ").concat(playerName);
            top.add(newScore);
        }
        catch (FileNotFoundException ex){
            new ErrorWindow("Can't read \"HighScoresTable.txt\" file.\n");
            dispose();
        }

        BasicComparator bc = new BasicComparator();
        Collections.sort(top, bc);
        if(top.size() == 11) {
            top.remove(top.size()-1);
        }

        try (FileWriter highScoresTableFW = new FileWriter("HighScoresTable.txt")){
            for(String elem: top){
                highScoresTableFW.write(elem + "\n");
            }
        }
        catch (IOException ex){
            new ErrorWindow("New high score can't be wrote to \"HighScoresTable.txt\" file.\n");
            dispose();
        }
    }

    class OKButtonMouseListener extends MouseAdapter {
        public void mouseClicked(MouseEvent event) {
            goToMainMenu();
            if(playerWin){
                addRecord(playerScore, name.getText());
            }
        }
    }

    class OKButtonKeyListener extends KeyAdapter {
        public void keyTyped(KeyEvent event) {
            if(event.getKeyChar() == KeyEvent.VK_ENTER){
                goToMainMenu();
                if(playerWin){
                    addRecord(playerScore, name.getText());
                }
            }
        }
    }

    class BasicComparator implements Comparator<String> {
        @Override
        public int compare(String str1, String str2) {
            String[] ar1 = str1.split(" ");
            String[] ar2 = str2.split(" ");
            return (-1)*Integer.compare(Integer.parseInt(ar1[0]), Integer.parseInt(ar2[0]));
        }
    }
}
