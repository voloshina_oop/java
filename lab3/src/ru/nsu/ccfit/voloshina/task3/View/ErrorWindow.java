package ru.nsu.ccfit.voloshina.task3.View;

import javax.swing.*;
import java.awt.*;

public class ErrorWindow extends JFrame {
    private JLabel ErrorMsg = new JLabel();
    private ErrorWindow() {}

    public ErrorWindow(String msg){
        super("ERROR");

        //this.setBounds(500,250,300,250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //ErrorMsg.setFont(new Font("Serif", Font.BOLD, 20));
        ErrorMsg.setText(msg);
        add(ErrorMsg);

        pack();
        setVisible(true);
    }
}
