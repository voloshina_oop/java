package ru.nsu.ccfit.voloshina.task3.View;

import ru.nsu.ccfit.voloshina.task3.Exceptions.HighScoreFileAccessingException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.HashSet;
import java.util.Set;

public class Welcome extends JFrame{
    private JButton newButton = new JButton("New Game");
    private JButton recordsButton = new JButton("Records");
    private JButton settingsButton = new JButton("Settings");
    private JButton aboutButton = new JButton("About");
    private JButton exitButton = new JButton("Exit");

    public Welcome(){
        super("Tetris");
        this.setBounds(450,200,350,300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        GridBagLayout gbl = new GridBagLayout();
        GridBagConstraints c =  new GridBagConstraints();
        c.anchor = GridBagConstraints.CENTER;
        c.fill   = GridBagConstraints.NONE;
        c.gridheight = 1;
        c.gridwidth  = GridBagConstraints.REMAINDER;
        c.gridx = GridBagConstraints.RELATIVE;
        c.gridy = GridBagConstraints.RELATIVE;
        c.insets = new Insets(3, 20, 0, 20);
        c.ipadx = 0;
        c.ipady = 0;
        c.weightx = 0.0;
        c.weighty = 0.0;
        gbl.setConstraints(newButton, c);
        c.ipadx = 19;
        gbl.setConstraints(recordsButton, c);
        c.ipadx = 15;
        gbl.setConstraints(settingsButton, c);
        c.ipadx = 32;
        gbl.setConstraints(aboutButton, c);
        c.ipadx = 48;
        gbl.setConstraints(exitButton, c);
        Container container = this.getContentPane();
        container.setLayout(gbl);

        newButton.addMouseListener(new NewGameButtonMouseListener());
        newButton.addKeyListener(new NewGameButtonKeyListener());

        exitButton.addMouseListener(new ExitButtonMouseListener());
        exitButton.addKeyListener(new ExitButtonKeyListener());

        recordsButton.addMouseListener(new RecordsButtonMouseListener());
        recordsButton.addKeyListener(new RecordsButtonKeyListener());

        settingsButton.addMouseListener(new SettingsButtonMouseListener());
        settingsButton.addKeyListener(new SettingsButtonKeyListener());

        container.add(newButton);
        container.add(recordsButton);
        container.add(settingsButton);
        container.add(aboutButton);
        container.add(exitButton);
    }

    public class NewGameButtonMouseListener extends MouseAdapter {
        public void mouseClicked(MouseEvent event) {
            try{
                new GUI();
                dispose();
            }
            catch (HighScoreFileAccessingException ex){
                ex.printStackTrace();
            }
        }
    }

    class NewGameButtonKeyListener extends KeyAdapter{
        public void keyTyped(KeyEvent event) {
            if(event.getKeyChar() == KeyEvent.VK_ENTER){
                try{
                    new GUI();
                    dispose();
                }
                catch (HighScoreFileAccessingException ex){
                    ex.printStackTrace();
                }
            }
        }
    }

    class ExitButtonMouseListener extends MouseAdapter{
        public void mouseClicked(MouseEvent event) {
            dispose();
        }
    }

    class ExitButtonKeyListener extends KeyAdapter{
        public void keyTyped(KeyEvent event) {
            if (event.getKeyChar() == KeyEvent.VK_ENTER) {
                dispose();
            }
        }
    }

    class RecordsButtonMouseListener extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent event) {
            new Records();
            dispose();
        }
    }

    class RecordsButtonKeyListener extends KeyAdapter{
        public void keyTyped(KeyEvent event) {
            if (event.getKeyChar() == KeyEvent.VK_ENTER) {
                new Records();
                dispose();
            }
        }
    }

    class SettingsButtonMouseListener extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent event) {
            new Settings();
            dispose();
        }
    }

    class SettingsButtonKeyListener extends KeyAdapter {
        public void keyTyped(KeyEvent event) {
            if (event.getKeyChar() == KeyEvent.VK_ENTER) {
                new Settings();
                dispose();
            }
        }
    }
}
