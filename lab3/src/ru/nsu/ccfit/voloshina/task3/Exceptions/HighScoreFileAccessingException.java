package ru.nsu.ccfit.voloshina.task3.Exceptions;

public class HighScoreFileAccessingException extends Exception {
    private HighScoreFileAccessingException(){}

    public HighScoreFileAccessingException(String arg){
        System.err.println("Error with access to \"HighScore.txt\" file. " + arg);
    }
}
