package ru.nsu.ccfit.voloshina.task3;

import ru.nsu.ccfit.voloshina.task3.Exceptions.HighScoreFileAccessingException;
import ru.nsu.ccfit.voloshina.task3.Model.CellStates;
import ru.nsu.ccfit.voloshina.task3.Model.Model;
import ru.nsu.ccfit.voloshina.task3.Model.Shapes.Shape;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class Controller {
    private Model model;
    private Timer timer = new Timer();
    private Fall fall = new Fall();
    private boolean gameOver = false;
    private int scoreForSpeedUp;
    private long timePeriod = 50;

    public Controller() throws HighScoreFileAccessingException {
        model = new Model();

        try(Scanner settingsReader = new Scanner(new FileReader("Settings.txt"))){
            while(settingsReader.hasNextLine()){
                String[] arr = settingsReader.nextLine().split(" ");
                if(arr[0].equals("scoreForSpeedUp")){
                    scoreForSpeedUp = Integer.parseInt(arr[2]);
                    break;
                }
            }
        }
        catch (FileNotFoundException ex){
            ex.printStackTrace();
            scoreForSpeedUp = 500;
        }

        timer.schedule(fall, 0, timePeriod);
    }

    public void stopTimer(){
        timer.cancel();
    }

    public void startTimer(){
        int fq = fall.getFallQuantity();
        int fc = fall.getFallCounter();
        timer = new Timer();
        fall = new Fall();
        fall.setFallQuantity(fq);
        fall.setFallCounter(fc);
        timer.schedule(fall, 0, timePeriod);
    }

    public ArrayList<ArrayList<CellStates>> getField() throws Exception{
        return model.drawField();
    }


    public int getFieldHeight(){
        return model.getHeightSize();
    }

    public int getFieldWidth(){
        return model.getWidthSize();
    }

    public Integer getLevel(){
        return model.getLevel();
    }

    public Long getCurScore(){
        return model.getCurScore();
    }

    public Long getHighScore(){
        return model.getHighScore();
    }

    public ArrayList<ArrayList<CellStates>> getNextShape(){
        return model.getNextShape();
    }

    public void rotateRight(){
        Shape curShape = model.getCurShape();
        curShape.turnRight();

        if(!model.checkUpdates()){
            curShape.turnLeft();
        }
    }

    public void rightShift(){
        int x = model.getCurInitialCell().getKey();
        int y = model.getCurInitialCell().getValue()+1;
        model.setCurInitialCell(x, y);

        if(!model.checkUpdates()){
            model.setCurInitialCell(x, y-1);
        }
    }

    public void leftShift(){
        int x = model.getCurInitialCell().getKey();
        int y = model.getCurInitialCell().getValue()-1;
        model.setCurInitialCell(x, y);

        if(!model.checkUpdates()){
            model.setCurInitialCell(x, y+1);
        }
    }

    public void downShift() throws HighScoreFileAccessingException{
        int speedUp = 2;
        int x = model.getCurInitialCell().getKey()+1;
        int y = model.getCurInitialCell().getValue();
        model.setCurInitialCell(x, y);

        if(!model.checkUpdates()){
            model.setCurInitialCell(x-1, y);
            if(x <= 0){
                model.setHighScore();
                timer.cancel();
                gameOver = true;
                model.allNotify();
                return;
            }
            model.putShape();
            if(model.checkLine()){
                System.out.println("Score: " + model.getCurScore());
                if(model.getCurScore() % scoreForSpeedUp == 0){
                    model.setLevel(model.getLevel()+1);
                    int fallQuantity = fall.getFallQuantity();
                    if(fallQuantity - speedUp > 0){
                        fall.setFallQuantity(fallQuantity - speedUp);
                        System.out.println("Current quantity: " + fall.getFallQuantity());
                    }
                }
            }
            model.getNewShapes();
            model.setCurInitialCell(-1, (model.getWidthSize() /2 - model.getCurShape().getRectSize()/2));
        }
    }

    public void subscribeToModel(Listener listener){
        model.subscribe(listener);
    }

    public boolean isGameOver() {
        return gameOver;
    }

    class Fall extends TimerTask {
        private int fallCounter = 0;
        private int fallQuantity = 20;

        int getFallQuantity() {
            return fallQuantity;
        }

        public int getFallCounter() {
            return fallCounter;
        }

        void setFallQuantity(int fallQuantity) {
            this.fallQuantity = fallQuantity;
        }

        public void setFallCounter(int fallCounter) {
            this.fallCounter = fallCounter;
        }

        @Override
        public void run(){
            model.allNotify();

            if(fallCounter >= fallQuantity){
                fallCounter = 0;
                try {
                    downShift();
                }
                catch (HighScoreFileAccessingException ex){
                    ex.printStackTrace();
                }
            }
            else{
                fallCounter++;
            }
        }
    }
}


