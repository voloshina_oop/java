package ru.nsu.ccfit.voloshina.task3;

public interface Listener {
    public void update();
}
