package ru.nsu.ccfit.voloshina.task2;

import ru.nsu.ccfit.voloshina.task2.Exceptions.ConfigException;
import ru.nsu.ccfit.voloshina.task2.Operations.StackOP;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Fabric {
    private Properties prop;
    private String configureFileName;
    private static Fabric ourInstance = new Fabric();
    private Map<String, StackOP> operations = new HashMap<>();

    public static Fabric getInstance() {
        return ourInstance;
    }

    private Fabric(){

    }

    public void configure(String filename) throws ConfigException{
        prop = new Properties();
        configureFileName = filename;
        ClassLoader cl = ClassLoader.getSystemClassLoader();
        try{
            prop.load(cl.getResourceAsStream(filename));
        }
        catch (IOException ex){
            String[] args = new String[2];
            args[0] = configureFileName;
            args[1] = "Loading config file error";
            throw new ConfigException(args);
        }
    }

    public StackOP getOperation(String classname)throws ConfigException{
        StackOP op;
        Object ob;
        if(((op = operations.get(classname)) != null)&&(!classname.equals("["))) return op;
        try{
            String name = prop.getProperty(classname);
            Class cl = Class.forName(name);
            ob = cl.getConstructor().newInstance();
            if(!(ob instanceof StackOP)) {
                String[] args = new String[2];
                args[0] = configureFileName;
                args[1] = "Unknown operation";
                throw new ConfigException(args);
            }
            op = (StackOP)ob;
            operations.put(classname, op);
            return op;
        }
        catch(ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | NullPointerException ex) {
            String[] args = new String[3];
            args[0] = configureFileName;
            args[1] = "Loading class ";
            args[2] = classname;
            ex.printStackTrace();
            throw new ConfigException(args);
        }
    }
}
