package ru.nsu.ccfit.voloshina.task2;

import ru.nsu.ccfit.voloshina.task2.Exceptions.ConfigException;
import ru.nsu.ccfit.voloshina.task2.Exceptions.WorkerExc.WorkerException;
import ru.nsu.ccfit.voloshina.task2.Operations.StackOP;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Parser {

    Parser(){}

    public static List<Pair<StackOP,ArrayList<String>>> read(Scanner source) throws ConfigException, WorkerException {
        List<Pair<StackOP, ArrayList<String>>> pairList = new ArrayList<>();

        while (source.hasNext()) {
            StackOP op;
            String curArg;
            if(source.hasNextDouble()){
                curArg = "push";
            }
            else {
                curArg = source.next();
                curArg = curArg.toLowerCase();
            }
            if(curArg.charAt(0) == '#'){
                source.nextLine();
                continue;
            }
            op = Fabric.getInstance().getOperation(curArg);
            pairList.add(new Pair<>(op, op.getArgs(source)));
        }
        return pairList;
    }
}
