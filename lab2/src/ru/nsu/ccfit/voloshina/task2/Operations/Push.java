package ru.nsu.ccfit.voloshina.task2.Operations;

import ru.nsu.ccfit.voloshina.task2.Context;
import ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc.HandleException;
import ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc.WrongArgumentException;
import ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc.WrongComandsOrderException;
import ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc.WrongDefinitionException;
import ru.nsu.ccfit.voloshina.task2.Exceptions.WorkerExc.WorkerFormatException;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Push implements StackOP {

    public Push(){

    }

    private Double isNumber(String string){
        Double d;
        try
        {
            d = Double.parseDouble(string);
        }
        catch(NumberFormatException ex){
            return null;
        }
        return d;
    }

    @Override
    public void execute(Context context, ArrayList<String> args)throws HandleException{
        Double number = isNumber(args.get(0));
        if((number == null)) {
            try {
                number = context.findDefinition(args.get(0));
            } catch (WrongDefinitionException ex) {
                throw new WrongArgumentException(this.getClass().getName());
            }
        }
        try {
            context.push(number);
        } catch (EmptyStackException e) {
            throw new WrongComandsOrderException();
        }
    }

    @Override
    public ArrayList<String> getArgs(Scanner source) throws WorkerFormatException{
        ArrayList<String> args;
        try {
            args = new ArrayList<>() {{
                add(source.next());
            }};
        } catch(NoSuchElementException ex){
            throw new WorkerFormatException("push");
        }
        return args;
    }
}
