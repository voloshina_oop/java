package ru.nsu.ccfit.voloshina.task2.Operations;

import ru.nsu.ccfit.voloshina.task2.Context;
import ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc.HandleException;
import ru.nsu.ccfit.voloshina.task2.Exceptions.WorkerExc.WorkerException;

import java.util.ArrayList;
import java.util.Scanner;

public interface StackOP {
    void execute(Context context, ArrayList<String> args) throws HandleException;
    ArrayList<String> getArgs(Scanner source) throws WorkerException;
}
