package ru.nsu.ccfit.voloshina.task2.Operations;

import ru.nsu.ccfit.voloshina.task2.Context;
import ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc.WrongComandsOrderException;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Scanner;

public class Rot implements StackOP {
    public Rot() {}

    @Override
    public void execute(Context context, ArrayList<String> args) throws WrongComandsOrderException {
        try {
            double c = context.pop();
            double b = context.pop();
            double a = context.pop();
            context.push(b);
            context.push(c);
            context.push(a);
        }
        catch (EmptyStackException ex){
            throw new WrongComandsOrderException();
        }
    }

    @Override
    public ArrayList<String> getArgs(Scanner source) {
        return null;
    }
}
