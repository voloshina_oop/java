package ru.nsu.ccfit.voloshina.task2.Operations;

import ru.nsu.ccfit.voloshina.task2.Context;
import ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc.WrongComandsOrderException;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Scanner;

public class Dup implements StackOP {
    public Dup() {}

    @Override
    public ArrayList<String> getArgs(Scanner source) {
        return null;
    }

    @Override
    public void execute(Context context, ArrayList<String> args) throws WrongComandsOrderException{
        try {
            double a = context.peek();
            context.push(a);
        } catch (EmptyStackException ex){
            throw new WrongComandsOrderException();
        }
    }
}
