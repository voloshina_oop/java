package ru.nsu.ccfit.voloshina.task2.Operations;

import ru.nsu.ccfit.voloshina.task2.Context;

import java.util.ArrayList;
import java.util.Scanner;

public class Less implements StackOP{
    public Less() {}

    @Override
    public ArrayList<String> getArgs(Scanner source) {
        return null;
    }

    @Override
    public void execute(Context context, ArrayList<String> args) {
        double a = context.pop();
        double b = context.pop();
        context.push(Integer.valueOf(b < a ? 1 : 0).doubleValue());
    }
}
