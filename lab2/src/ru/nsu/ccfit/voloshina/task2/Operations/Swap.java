package ru.nsu.ccfit.voloshina.task2.Operations;

import ru.nsu.ccfit.voloshina.task2.Context;
import ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc.WrongComandsOrderException;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Scanner;

public class Swap implements StackOP {
    public Swap() {}

    @Override
    public void execute(Context context, ArrayList<String> args) throws WrongComandsOrderException{
        try {
            double a = context.pop();
            double b = context.pop();
            context.push(a);
            context.push(b);
        }
        catch (EmptyStackException ex){
            throw new WrongComandsOrderException();
        }
    }

    @Override
    public ArrayList<String> getArgs(Scanner source) {
        return null;
    }
}
