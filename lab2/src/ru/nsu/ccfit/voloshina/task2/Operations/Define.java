package ru.nsu.ccfit.voloshina.task2.Operations;

import ru.nsu.ccfit.voloshina.task2.Context;
import ru.nsu.ccfit.voloshina.task2.Exceptions.WorkerExc.WorkerFormatException;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Define implements StackOP {
    public Define() {}

    @Override
    public ArrayList<String> getArgs(Scanner source) throws WorkerFormatException{
        ArrayList<String> args;
        try{
            args = new ArrayList<String>() {{
                add(source.next());
            }};
            if(source.hasNextDouble()) args.add(source.next());
            else throw new WorkerFormatException("define");
        } catch(NoSuchElementException ex){
            throw new WorkerFormatException("define");
        }
        return args;
    }

    @Override
    public void execute(Context context, ArrayList<String> args) {
        context.addDefinition(args.get(0), Double.parseDouble(args.get(1)));
    }
}
