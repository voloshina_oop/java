package ru.nsu.ccfit.voloshina.task2.Operations;

import ru.nsu.ccfit.voloshina.task2.Pair;
import ru.nsu.ccfit.voloshina.task2.Context;
import ru.nsu.ccfit.voloshina.task2.Exceptions.ConfigException;
import ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc.HandleException;
import ru.nsu.ccfit.voloshina.task2.Exceptions.WorkerExc.UnrecWorkerException;
import ru.nsu.ccfit.voloshina.task2.Exceptions.WorkerExc.WorkerException;
import ru.nsu.ccfit.voloshina.task2.Fabric;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Loop implements StackOP {
    private List<Pair<StackOP,ArrayList<String>>> ops;

    public Loop() {
        ops = new ArrayList<>();
    }


    @Override
    public ArrayList<String> getArgs(Scanner source) throws WorkerException {
        while (!source.hasNext("]")){
            StackOP op;
            String curOp;
            if(source.hasNextDouble()){
                curOp = "push";
            }
            else {
                curOp = source.next();
                curOp = curOp.toLowerCase();
            }
            if(curOp.equals("#")){
                source.nextLine();
                continue;
            }
            try {
                op = Fabric.getInstance().getOperation(curOp);
            } catch (ConfigException ex){
                throw new UnrecWorkerException(curOp);
            }
            ops.add(new Pair<>(op, op.getArgs(source)));
        }
        source.next();
        return null;
    }

    @Override
    public void execute(Context context, ArrayList<String> args) throws HandleException{
        while(!(context.peek()==0)){
            context.pop();
            for(Pair<StackOP,ArrayList<String>> elem: ops) {
                elem.getKey().execute(context, elem.getValue());
            }
        }
        context.pop();
    }
}
