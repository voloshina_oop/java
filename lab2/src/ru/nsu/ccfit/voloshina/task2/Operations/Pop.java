package ru.nsu.ccfit.voloshina.task2.Operations;

import ru.nsu.ccfit.voloshina.task2.Context;
import java.util.ArrayList;
import java.util.Scanner;

public class Pop implements StackOP {
    public Pop(){}

    @Override
    public void execute(Context context, ArrayList<String> args) {
        context.pop();
    }

    @Override
    public ArrayList<String> getArgs(Scanner source) {
        return null;
    }
}
