package ru.nsu.ccfit.voloshina.task2.Operations;

import ru.nsu.ccfit.voloshina.task2.Context;
import ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc.WrongComandsOrderException;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Scanner;

public class Plus implements StackOP {

    public Plus(){}

    @Override
    public void execute(Context context, ArrayList<String> args)throws WrongComandsOrderException{  //TODO: throws HandleExc??
        try {
            context.push(context.pop() + context.pop());
        }
        catch (EmptyStackException e){
            throw new WrongComandsOrderException();
        }
    }

    @Override
    public ArrayList<String> getArgs(Scanner source) {
        return null;
    }
}
