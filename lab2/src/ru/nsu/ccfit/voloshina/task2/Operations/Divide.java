package ru.nsu.ccfit.voloshina.task2.Operations;

import ru.nsu.ccfit.voloshina.task2.Context;
import ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc.WrongComandsOrderException;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Scanner;

public class Divide implements StackOP {
    public Divide(){}

    @Override
    public void execute(Context context, ArrayList<String> args) throws WrongComandsOrderException {
        try {
            context.push(context.pop()/context.pop());
        }
        catch (EmptyStackException ex){
            throw new WrongComandsOrderException();
        }
    }

    @Override
    public ArrayList<String> getArgs(Scanner source) {
        return null;
    }
}
