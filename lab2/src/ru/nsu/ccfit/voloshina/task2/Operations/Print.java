package ru.nsu.ccfit.voloshina.task2.Operations;

import ru.nsu.ccfit.voloshina.task2.Context;

import java.util.ArrayList;
import java.util.Scanner;

public class Print implements StackOP{
    public Print(){}

    public void execute(Context context, ArrayList<String> args){
        System.out.println(context.peek());
    }

    @Override
    public ArrayList<String> getArgs(Scanner source) {
        return null;
    }
}
