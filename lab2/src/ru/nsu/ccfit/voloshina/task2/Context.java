package ru.nsu.ccfit.voloshina.task2;

import ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc.WrongDefinitionException;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Stack;

public class Context {
    private Stack<Double> stack;
    private HashMap<String, Double> definitions;

    Context(){
        stack = new Stack<>();
        definitions = new HashMap<>();
    }

    public void push(Double number)throws EmptyStackException {
        stack.push(number);
    }

    public Double pop()throws EmptyStackException{
        return stack.pop();
    }

    public Double peek()throws EmptyStackException{
        return stack.peek();
    }

    public void addDefinition(String key, Double value){
        definitions.put(key, value);
    }

    public Double findDefinition(String key) throws WrongDefinitionException{
        Double value = definitions.get(key);
        if(value == null) throw new WrongDefinitionException(key);
        else return value;
    }

}
