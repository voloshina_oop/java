package ru.nsu.ccfit.voloshina.task2.Tests;

import org.junit.Assert;
import org.junit.Test;
import ru.nsu.ccfit.voloshina.task2.ConsoleInterceptor;
import ru.nsu.ccfit.voloshina.task2.StackCalc;


public class Tests extends Assert {

    @Test
    public void testSqrt()throws Exception{
        String[] args = {"input1.txt"};
        final StackCalc stackCalc = new StackCalc();

        String result = ConsoleInterceptor.copyOut(() -> stackCalc.count(args, "config.properties"));
        assertEquals("2.0\n", result);
    }

    @Test
    public void testPlus()throws Exception{
        String[] args = {"input2.txt"};
        final StackCalc stackCalc = new StackCalc();

        String result = ConsoleInterceptor.copyOut(() -> stackCalc.count(args, "config.properties"));
        assertEquals("5.0\n", result);
    }

    @Test
    public void testDup()throws Exception{
        String[] args = {"input3.txt"};
        final StackCalc stackCalc = new StackCalc();

        String result = ConsoleInterceptor.copyOut(() -> stackCalc.count(args, "config.properties"));
        assertEquals("5.0\n4.0\n3.0\n2.0\n1.0\n", result);
    }

    @Test
    public void testSwap()throws Exception{
        String[] args = {"input4.txt"};
        final StackCalc stackCalc = new StackCalc();

        String result = ConsoleInterceptor.copyOut(() -> stackCalc.count(args, "config.properties"));
        assertEquals("120.0\n", result);
    }

    @Test
    public void testEmbeddedLoop()throws Exception{
        String[] args = {"input6.txt"};
        final StackCalc stackCalc = new StackCalc();

        String result = ConsoleInterceptor.copyOut(() -> stackCalc.count(args, "config.properties"));
        assertEquals("5.0\n4.0\n3.0\n2.0\n1.0\n4.0\n3.0\n2.0\n1.0\n3.0\n2.0\n1.0\n2.0\n1.0\n1.0\n", result);
    }

    @Test
    public void testLess()throws Exception{
        String[] args = {"input7.txt"};
        final StackCalc stackCalc = new StackCalc();

        String result = ConsoleInterceptor.copyOut(() -> stackCalc.count(args, "config.properties"));
        assertEquals("1.0\n", result);
    }

    @Test
    public void testLoop()throws Exception{
        String[] args = {"input8.txt"};
        final StackCalc stackCalc = new StackCalc();

        String result = ConsoleInterceptor.copyOut(() -> stackCalc.count(args, "config.properties"));
        assertEquals("5.0\n", result);
    }

    @Test
    public void testLoop2()throws Exception{
        String[] args = {"input9.txt"};
        final StackCalc stackCalc = new StackCalc();

        String result = ConsoleInterceptor.copyOut(() -> stackCalc.count(args, "config.properties"));
        assertEquals("5.0\n", result);
    }

    @Test
    public void testDefine()throws Exception{
        String[] args = {"input10.txt"};
        final StackCalc stackCalc = new StackCalc();

        String result = ConsoleInterceptor.copyOut(() -> stackCalc.count(args, "config.properties"));
        assertEquals("", result);
    }
}
