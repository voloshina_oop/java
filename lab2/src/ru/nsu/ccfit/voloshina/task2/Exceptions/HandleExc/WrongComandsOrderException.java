package ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc;

public class WrongComandsOrderException extends HandleException {

    public WrongComandsOrderException(){}

    public void print(){
        System.err.println("ERROR: Wrong commands order. Program can't be executed. It will be terminated.");
    }
}
