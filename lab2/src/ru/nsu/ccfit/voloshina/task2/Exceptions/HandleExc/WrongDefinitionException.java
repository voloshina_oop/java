package ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc;

public class WrongDefinitionException extends HandleException {
    private String arg;

    private WrongDefinitionException(){}

    public WrongDefinitionException(String str){
        arg = str;
    }

    public void print(){
        System.err.println("ERROR: Define for variable "+arg+" not found. Program can't be executed. It will be terminated.");
    }
}
