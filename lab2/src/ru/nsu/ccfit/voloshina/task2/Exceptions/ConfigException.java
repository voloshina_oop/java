package ru.nsu.ccfit.voloshina.task2.Exceptions;

public class ConfigException extends MainException {
    private String[] arg;

    private ConfigException(){}

    public ConfigException(String[] str){
        arg = str;
    }

    public void print(){
        System.err.println("ERROR with config file "+arg[0]+": "+arg[1]+"\""+arg[2]+"\""+" error.\nProgram can't be executed. It will be terminated.");
    }

    public String getClassname(){
        return arg[2];
    }
}
