package ru.nsu.ccfit.voloshina.task2.Exceptions.WorkerExc;

public class UnrecWorkerException extends WorkerException {
    private String arg;

    private UnrecWorkerException(){}

    public UnrecWorkerException(String str){
        arg = str;
    }

    public void print(){
        System.err.println("ERROR: Unrecognised operation: "+arg+". Program can't be executed. It will be terminated.");
    }
}
