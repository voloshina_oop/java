package ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc;

public class WrongArgumentException extends HandleException {
    private String arg;

    private WrongArgumentException(){}

    public WrongArgumentException(String str){
        arg = str;
    }

    public void print(){
        System.err.println("ERROR: Wrong argument for operation "+arg+". Program can't be executed. It will be terminated.");
    }
}
