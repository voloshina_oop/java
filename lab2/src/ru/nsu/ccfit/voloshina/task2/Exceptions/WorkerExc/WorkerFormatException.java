package ru.nsu.ccfit.voloshina.task2.Exceptions.WorkerExc;

public class WorkerFormatException extends WorkerException{
    private String arg;

    private WorkerFormatException(){}

    public WorkerFormatException(String str){
        arg = str;
    }

    public void print(){
        System.err.println("ERROR: Invalid operation "+arg+" format. Program can't be executed. It will be terminated.");
    }
}
