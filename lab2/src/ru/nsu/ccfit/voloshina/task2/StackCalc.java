package ru.nsu.ccfit.voloshina.task2;

import ru.nsu.ccfit.voloshina.task2.Exceptions.ConfigException;
import ru.nsu.ccfit.voloshina.task2.Exceptions.MainException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;
import java.util.Scanner;

public class StackCalc {
    public StackCalc(){ }

    public void count(String[] args, String configFileName)throws MainException, IOException{
        Fabric.getInstance().configure(configFileName);
        Scanner source;
        if((args.length == 0)||(Objects.equals(args[0], "-")))
            source = new Scanner(System.in);
        else try {
            source = new Scanner(new File(args[0]));
        }
        catch (FileNotFoundException ex){
            System.err.println("Input file named "+args[0]+" not found.");
            throw new IOException();
        }
        Handler.handle(Parser.read(source));
    }

    public void reconfugure(String new_configFileName)throws ConfigException {
        Fabric.getInstance().configure(new_configFileName);
    }
}
