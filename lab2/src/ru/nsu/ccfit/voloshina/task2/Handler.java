package ru.nsu.ccfit.voloshina.task2;

import ru.nsu.ccfit.voloshina.task2.Exceptions.HandleExc.HandleException;
import ru.nsu.ccfit.voloshina.task2.Operations.StackOP;

import java.util.ArrayList;
import java.util.List;

public class Handler {
    Handler(){

    }

    static void handle(List<Pair<StackOP,ArrayList<String>>> opList) throws HandleException{
        Context context = new Context();
        for(Pair<StackOP,ArrayList<String>> elem: opList) {
            elem.getKey().execute(context, elem.getValue());
        }
    }
}
