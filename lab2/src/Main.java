import ru.nsu.ccfit.voloshina.task2.Exceptions.MainException;
import ru.nsu.ccfit.voloshina.task2.StackCalc;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        try {
            StackCalc stackCalc = new StackCalc();
            stackCalc.count(args, "config.properties");
        }
        catch (IOException ex){
            System.out.println("");
        }
        catch (MainException ex){
            ex.printStackTrace();
            ex.print();
        }
    }
}
