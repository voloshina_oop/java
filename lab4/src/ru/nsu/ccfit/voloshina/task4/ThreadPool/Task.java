package ru.nsu.ccfit.voloshina.task4.ThreadPool;

public interface Task {
    //String getName();   //TODO: need?
    void performWork() throws InterruptedException;
}
