package ru.nsu.ccfit.voloshina.task4.OS;

public interface Subscriber {
    void update();
}
