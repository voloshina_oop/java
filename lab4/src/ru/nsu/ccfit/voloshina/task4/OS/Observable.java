package ru.nsu.ccfit.voloshina.task4.OS;

import ru.nsu.ccfit.voloshina.task4.Exceptions.NoSubException;

public interface Observable {
    void addSub(Subscriber sub);
    void removeSub(Subscriber sub) throws NoSubException;    //TODO: exc
    void notifySubs();
}
