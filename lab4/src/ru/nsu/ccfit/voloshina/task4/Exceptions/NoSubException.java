package ru.nsu.ccfit.voloshina.task4.Exceptions;

import ru.nsu.ccfit.voloshina.task4.OS.*;

public class NoSubException extends Exception {
    public NoSubException(Observable o, Subscriber s){
        super(o.toString() + " :no such subscriber (" + s.toString() + ")");
    }
}
